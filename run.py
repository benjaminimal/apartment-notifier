#!/usr/bin/env python3
import os
import sys
import logging

import requests
from bs4 import BeautifulSoup
from telegram.ext import Updater

# Logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(message)s',
                    level=logging.INFO)

# Bruno-Marek-Allee 19-23
ROOMS = 4
BASE_URL = 'https://www.oevw.at'
ENDPOINT = BASE_URL + '/wohnbauprojekt/3239862'

# Telegram
try:
    TOKEN = os.environ['APARTMENT_NOTIFIER_TELEGRAM_TOKEN']
    CHAT_ID = os.environ['APARTMENT_NOTIFIER_TELEGRAM_CHAT_ID']
except KeyError as err:
    logging.error('Missing environment variable {}'.format(err.args[0]))
    sys.exit(1)
UPDATER = Updater(token=TOKEN, use_context=True)
MSG_PATTERN = 'OMG {} Zimmer Wohnung in der Bruni-Marek-Allee 19-23 frei.\n{}'

def check_apartments(context):
    logging.info('Fetching {}'.format(ENDPOINT))
    response = requests.get(ENDPOINT)
    soup = BeautifulSoup(response.text, 'html.parser')
    table = soup.find('table', {'class': 'sub-realty-list'})
    for row in table.find('tbody').find_all('tr'):
        rooms = int(row.contents[5].text)
        if rooms >= ROOMS:
            endpoint = row.contents[2].a['href']
            url = BASE_URL + endpoint
            logging.info('Found {} room apartment at {}'.format(rooms, url))
            context.bot.send_message(CHAT_ID, MSG_PATTERN.format(rooms, url))


if __name__ == '__main__':
    UPDATER.job_queue.run_repeating(check_apartments, interval=60 * 60, first=0)
    UPDATER.start_polling()
    UPDATER.idle()
